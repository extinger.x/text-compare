import {Injectable} from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class TextCompareService {

  constructor() {
  }

  compareStrings(string1: string, string2: string) {
    const s1_words = string1.split(' ');
    const s2_words = string2.split(' ');

    const differences = this.rec_str(s1_words, s2_words, [], [], 0, 0);

    const a1 = differences[0];
    const a2 = differences[1];

    let s1 = '';
    let s2 = '';

    let b = false, bl: number = -2, p = false;
    for (const i in s1_words) {
      if (p) s1 += ' ';
      else p = true;

      if (typeof (a1[i]) !== 'undefined') {
        b = true;
        // @ts-ignore
        if (bl != i - 1) {
          s1 += '<span class="bg-danger">';
        }
        // @ts-ignore
        bl = i;

      } else {
        b = false;
        // @ts-ignore
        if (bl == i - 1) {
          s1 += '</span>';
        }
      }
      s1 += s1_words[i];
    }
    // @ts-ignore
    if (bl == i) {
      s1 += '</span>';
    }
    b = false;
    bl = -2;
    p = false;
    for (var i in s2_words) {
      if (p) s2 += ' ';
      else p = true;

      if (typeof (a2[i]) !== 'undefined') {
        b = true;
        // @ts-ignore
        if (bl != i - 1) {
          s2 += '<span class="bg-success">';
        }
        // @ts-ignore
        bl = i;

      } else {
        b = false;
        // @ts-ignore
        if (bl == i - 1) {
          s2 += '</span>';
        }
      }
      s2 += s2_words[i];
    }
    // @ts-ignore
    if (bl == i) {
      s2 += '</span>';
    }

    return [s1, s2];
  }

  // @ts-ignore
  private rec_str(s1, s2, a1, a2, sm1, sm2) {
    var n1 = s1.length;
    var n2 = s2.length;
    var n_min = n1 > n2 ? n2 : n1;
    var n_max = n1 > n2 ? n1 : n2;
    var id = 0;
    for (var i = 0; i < n_max; i++) {
      if (s1[i] !== s2[i]) {
        for (var j = i * 2 + 2, n_max2 = n_max * 2; j < n_max2; j++) {
          id = Math.floor(j / 2);
          if (j % 2 === 0) {
            if (id >= n1) continue;
            for (var k = i; k <= id; k++) {
              if (k >= n2) break;
              if (s2[k].length < 3) continue;//игнорирование предлогов для слов
              if (s2[k] === s1[id]) {
                //занесение ключей различающихся слов в a1,a2 с учетом смещений sm1 и sm2
                for (var ii = i; ii < id; ii++)
                  a1[ii + sm1] = true;
                for (var ii = i; ii < k; ii++)
                  a2[ii + sm2] = true;
                //наращение смещений согласно найденному совпадению
                sm1 += id;
                sm2 += k;
                //и вызов функции с отрезками массивов слов, начиная с совпавших элементов
                return this.rec_str(s1.slice(id), s2.slice(k), a1, a2, sm1, sm2);
              }
            }
          } else {
            if (id >= n2) continue;
            for (var k = i; k < id; k++) {
              if (k >= n1) break;
              if (s1[k].length < 3) continue;//игнорирование предлогов для слов
              if (s1[k] === s2[id]) {
                //занесение ключей различающихся слов в a1,a2 с учетом смещений sm1 и sm2
                for (var ii = i; ii < k; ii++)
                  a1[ii + sm1] = true;
                for (var ii = i; ii < id; ii++)
                  a2[ii + sm2] = true;
                //наращение смещений согласно найденному совпадению
                sm1 += k;
                sm2 += id;
                //и вызов функции с отрезками массивов слов, начиная с совпавших элементов
                return this.rec_str(s1.slice(k), s2.slice(id), a1, a2, sm1, sm2);
              }
            }
          }
        }
        //здесь, если не выявлено совпадений последующих слов, заполнение массивов с различиями слов оставшимися ключами.
        for (var ii = i, cc = n1; ii < cc; ii++)
          a1[ii + sm1] = ii + sm1;
        for (var ii = i, cc = n2; ii < cc; ii++)
          a2[ii + sm2] = ii + sm2;
        //и возвращение массива с массивами различий.
        return [a1, a2];
      }
    }
    //дубль вывода на случай, когда один из массивов закончился
    return [a1, a2];
  }

}
