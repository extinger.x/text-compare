import { Component } from '@angular/core';
import {TextCompareService} from "./services/text-compare.service";

declare const jQuery: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  text1: string = '';
  text2: string = '';

  title = 'text-compare';
  result: string[] | null = null;

  constructor(private tcs: TextCompareService) {
  }

  doCompare() {
    const res = this.tcs.compareStrings(this.text1, this.text2)
    console.log(res);
    this.result = res;
  }

  jQ(t: any) {
    return jQuery(t);
  }
}
